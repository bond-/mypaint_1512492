//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyPaint.rc
//
#define IDC_MYICON                      2
#define IDD_MYPAINT_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYPAINT                     107
#define IDI_SMALL                       108
#define IDC_MYPAINT                     109
#define IDR_MAINFRAME                   128
#define ID_N32771                       32771
#define ID_N32772                       32772
#define ID_N32773                       32773
#define ID_SHAPES_LINE                  32774
#define ID_SHAPES_ELL                   32775
#define ID_SHAPES_RECTANGLE             32776
#define ID_SHAPES_ELLIPSE               32777
#define New                             32778
#define ID_FILE_NEW                     32779
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
