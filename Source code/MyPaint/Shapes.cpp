#include "MyPaint.h"


CShape::CShape()
{
}


CShape::~CShape()
{
}

///////////////////////////////////////////////////////////////////////

CLine::CLine()
{
}

void CLine::Draw(HDC hdc)
{
	gHpen = CreatePen(0, sSize, sColor);
	SelectObject(hdc, gHpen);
	MoveToEx(hdc, a.x, a.y, NULL);
	LineTo(hdc, b.x, b.y);
	DeleteObject(gHpen);
}

CShape* CLine::Create()
{
	return new CLine;
}

void CLine::SetData(int ax, int ay, int bx, int by)
{
	a.x = ax;
	a.y = ay;
	b.x = bx;
	b.y = by;
}

CLine::~CLine()
{
}


///////////////////////////////////////////////////////////////////////

CRectangle::CRectangle()
{
}

void CRectangle::Draw(HDC hdc)
{
	gHpen = CreatePen(0, sSize, sColor);
	SelectObject(hdc, gHpen);
	Rectangle(hdc, a.x, a.y, b.x, b.y);
	DeleteObject(gHpen);
}

CShape* CRectangle::Create()
{
	return new CRectangle;
}

void CRectangle::SetData(int ax, int ay, int bx, int by)
{
	a.x = ax;
	a.y = ay;
	b.x = bx;
	b.y = by;
}


CRectangle::~CRectangle()
{
}

///////////////////////////////////////////////////////////////////////

CEllipse::CEllipse()
{
}

void CEllipse::Draw(HDC hdc) {
	gHpen = CreatePen(0, sSize, sColor);
	SelectObject(hdc, gHpen);
	Ellipse(hdc, a.x, a.y, b.x, b.y);
	DeleteObject(gHpen);
}

CShape* CEllipse::Create() { return new CEllipse; }

void CEllipse::SetData(int ax, int ay, int bx, int by)
{
	a.x = ax;
	a.y = ay;
	b.x = bx;
	b.y = by;
}


CEllipse::~CEllipse()
{
}
