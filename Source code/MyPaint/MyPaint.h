﻿#pragma once

#include "resource.h"
#include <iostream>
#include <vector>
#include <windowsx.h>
#include <windowsX.h>
#include <assert.h>
#include <comdef.h>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "comctl32.lib")

#define LINE 1
#define RECTANGLE 2
#define ELLIPSE 3


extern bool isDrawing;
extern HWND gHPaint;
extern POINT pCurrent, pEnd;
extern HPEN gHpen;
extern int sizeOfPen;		
extern int iChoose;				
extern COLORREF iColor;				




class CShape
{
protected:
	POINT a, b;
	COLORREF sColor = RGB(0, 0, 0);
	int sSize = 1;
public:
	virtual void Draw(HDC hdc) = 0;
	virtual CShape* Create() = 0;
	virtual void SetData(int a, int b, int c, int d) = 0;
	CShape();
	~CShape();
};



class CRectangle : public CShape
{
public:
	CRectangle();
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int ax, int ay, int bx, int by);
	~CRectangle();
};

class CLine : public CShape
{
public:
	CLine();
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int ax, int ay, int bx, int by);
	~CLine();
};


class CEllipse : public CShape
{
public:
	CEllipse();
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int ax, int ay, int bx, int by);
	~CEllipse();
};


