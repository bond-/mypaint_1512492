# My Paint - vẽ đơn giản

##Họ tên: Nguyễn Xuân Tân     MSSV:1512492


###Tính năng đã làm được:

1. Đường thẳng (line). Dùng hàm MoveToEx và LineTo.

2. Hình chữ nhật (rectangle). Dùng hàm Rectangle. Nếu giữ phím Shift sẽ vẽ hình vuông (Square)

3. Hình Ellipse. Dùng hàm Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)

Nét vẽ màu đen, kích thước 1


####Link bitbucket: https://bitbucket.org/bond-/mypaint_1512492/ - https://bond-@bitbucket.org/bond-/mypaint_1512492.git
####Link youtube: https://youtu.be/20Jk5QAXnd0